#ifndef INCLUDE_FILELOADER_H
#define INCLUDE_FILELOADER_H
#pragma once
#include "TYPES.h"

class TLoader
{
public:
	static MATRIX LoadFromFile(std::string FilePath, bool IsonlyX);
	static void SaveToFile(std::string FilePath, MATRIX Data);
};
#endif