#pragma once
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

#include <iostream>
#include <string>
#include <cstring>
#include "FileLoader.h"
#include "../interpolationLib/interpolationLib.h"
#include <msclr\marshal_cppstd.h>

namespace interpolation {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace Interpolation;

	/// <summary>
	/// Summary for MainForm
	/// </summary>
	public ref class MainForm : public System::Windows::Forms::Form
	{
	public:
		MainForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MainForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::OpenFileDialog^  OFDInputGrid;
	protected:
	private: System::Windows::Forms::TextBox^  TBInputGrid;
	private: System::Windows::Forms::Button^  BInputGrid;
	private: System::Windows::Forms::RadioButton^  rStep;
	private: System::Windows::Forms::RadioButton^  rLine;
	private: System::Windows::Forms::RadioButton^  rQuad;
	private: System::Windows::Forms::Button^  BToInerpolate;
	private: System::Windows::Forms::Button^  BOutputFile;

	private: System::Windows::Forms::OpenFileDialog^  OFDToInterpolate;
	private: System::Windows::Forms::TextBox^  TBToInterpolate;
	private: System::Windows::Forms::TextBox^  TBOutput;
	private: System::Windows::Forms::SaveFileDialog^  SFDOutput;
	private: System::Windows::Forms::Button^  BInterpolate;
	private: System::Windows::Forms::Label^  lIntputGrid;
	private: System::Windows::Forms::Label^  lToInterpolate;
	private: System::Windows::Forms::Label^  lOutput;
	private: System::Windows::Forms::Label^  lStatus;
	private: System::Windows::Forms::ProgressBar^  progressBar1;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->OFDInputGrid = (gcnew System::Windows::Forms::OpenFileDialog());
			this->TBInputGrid = (gcnew System::Windows::Forms::TextBox());
			this->BInputGrid = (gcnew System::Windows::Forms::Button());
			this->rStep = (gcnew System::Windows::Forms::RadioButton());
			this->rLine = (gcnew System::Windows::Forms::RadioButton());
			this->rQuad = (gcnew System::Windows::Forms::RadioButton());
			this->BToInerpolate = (gcnew System::Windows::Forms::Button());
			this->BOutputFile = (gcnew System::Windows::Forms::Button());
			this->OFDToInterpolate = (gcnew System::Windows::Forms::OpenFileDialog());
			this->TBToInterpolate = (gcnew System::Windows::Forms::TextBox());
			this->TBOutput = (gcnew System::Windows::Forms::TextBox());
			this->SFDOutput = (gcnew System::Windows::Forms::SaveFileDialog());
			this->BInterpolate = (gcnew System::Windows::Forms::Button());
			this->lIntputGrid = (gcnew System::Windows::Forms::Label());
			this->lToInterpolate = (gcnew System::Windows::Forms::Label());
			this->lOutput = (gcnew System::Windows::Forms::Label());
			this->lStatus = (gcnew System::Windows::Forms::Label());
			this->progressBar1 = (gcnew System::Windows::Forms::ProgressBar());
			this->SuspendLayout();
			// 
			// OFDInputGrid
			// 
			this->OFDInputGrid->FileName = L"OFDInputGrid";
			// 
			// TBInputGrid
			// 
			this->TBInputGrid->Enabled = false;
			this->TBInputGrid->Location = System::Drawing::Point(95, 12);
			this->TBInputGrid->Name = L"TBInputGrid";
			this->TBInputGrid->Size = System::Drawing::Size(197, 20);
			this->TBInputGrid->TabIndex = 0;
			// 
			// BInputGrid
			// 
			this->BInputGrid->Location = System::Drawing::Point(298, 10);
			this->BInputGrid->Name = L"BInputGrid";
			this->BInputGrid->Size = System::Drawing::Size(46, 23);
			this->BInputGrid->TabIndex = 1;
			this->BInputGrid->Text = L"select";
			this->BInputGrid->UseVisualStyleBackColor = true;
			this->BInputGrid->Click += gcnew System::EventHandler(this, &MainForm::BInputGrid_Click);
			// 
			// rStep
			// 
			this->rStep->AutoSize = true;
			this->rStep->Checked = true;
			this->rStep->Location = System::Drawing::Point(12, 93);
			this->rStep->Name = L"rStep";
			this->rStep->Size = System::Drawing::Size(105, 17);
			this->rStep->TabIndex = 2;
			this->rStep->TabStop = true;
			this->rStep->Text = L"step interpolation";
			this->rStep->UseVisualStyleBackColor = true;
			// 
			// rLine
			// 
			this->rLine->AutoSize = true;
			this->rLine->Location = System::Drawing::Point(12, 116);
			this->rLine->Name = L"rLine";
			this->rLine->Size = System::Drawing::Size(110, 17);
			this->rLine->TabIndex = 3;
			this->rLine->TabStop = true;
			this->rLine->Text = L"linear interpolation";
			this->rLine->UseVisualStyleBackColor = true;
			// 
			// rQuad
			// 
			this->rQuad->AutoSize = true;
			this->rQuad->Location = System::Drawing::Point(12, 139);
			this->rQuad->Name = L"rQuad";
			this->rQuad->Size = System::Drawing::Size(123, 17);
			this->rQuad->TabIndex = 4;
			this->rQuad->TabStop = true;
			this->rQuad->Text = L"qadratic interpolation";
			this->rQuad->UseVisualStyleBackColor = true;
			// 
			// BToInerpolate
			// 
			this->BToInerpolate->Location = System::Drawing::Point(298, 35);
			this->BToInerpolate->Name = L"BToInerpolate";
			this->BToInerpolate->Size = System::Drawing::Size(46, 23);
			this->BToInerpolate->TabIndex = 5;
			this->BToInerpolate->Text = L"select";
			this->BToInerpolate->UseVisualStyleBackColor = true;
			this->BToInerpolate->Click += gcnew System::EventHandler(this, &MainForm::BToInerpolate_Click);
			// 
			// BOutputFile
			// 
			this->BOutputFile->Enabled = false;
			this->BOutputFile->Location = System::Drawing::Point(298, 61);
			this->BOutputFile->Name = L"BOutputFile";
			this->BOutputFile->Size = System::Drawing::Size(46, 23);
			this->BOutputFile->TabIndex = 6;
			this->BOutputFile->Text = L"select";
			this->BOutputFile->UseVisualStyleBackColor = true;
			this->BOutputFile->Click += gcnew System::EventHandler(this, &MainForm::BOutputFile_Click);
			// 
			// OFDToInterpolate
			// 
			this->OFDToInterpolate->FileName = L"OFDInputGrid";
			// 
			// TBToInterpolate
			// 
			this->TBToInterpolate->Enabled = false;
			this->TBToInterpolate->Location = System::Drawing::Point(95, 37);
			this->TBToInterpolate->Name = L"TBToInterpolate";
			this->TBToInterpolate->Size = System::Drawing::Size(197, 20);
			this->TBToInterpolate->TabIndex = 7;
			// 
			// TBOutput
			// 
			this->TBOutput->Enabled = false;
			this->TBOutput->Location = System::Drawing::Point(95, 63);
			this->TBOutput->Name = L"TBOutput";
			this->TBOutput->Size = System::Drawing::Size(197, 20);
			this->TBOutput->TabIndex = 8;
			// 
			// BInterpolate
			// 
			this->BInterpolate->Enabled = false;
			this->BInterpolate->Location = System::Drawing::Point(273, 87);
			this->BInterpolate->Name = L"BInterpolate";
			this->BInterpolate->Size = System::Drawing::Size(71, 23);
			this->BInterpolate->TabIndex = 9;
			this->BInterpolate->Text = L"interpolate";
			this->BInterpolate->UseVisualStyleBackColor = true;
			this->BInterpolate->Click += gcnew System::EventHandler(this, &MainForm::BInterpolate_Click);
			// 
			// lIntputGrid
			// 
			this->lIntputGrid->AutoSize = true;
			this->lIntputGrid->Location = System::Drawing::Point(12, 15);
			this->lIntputGrid->Name = L"lIntputGrid";
			this->lIntputGrid->Size = System::Drawing::Size(40, 13);
			this->lIntputGrid->TabIndex = 10;
			this->lIntputGrid->Text = L"grid file";
			// 
			// lToInterpolate
			// 
			this->lToInterpolate->AutoSize = true;
			this->lToInterpolate->Location = System::Drawing::Point(12, 40);
			this->lToInterpolate->Name = L"lToInterpolate";
			this->lToInterpolate->Size = System::Drawing::Size(72, 13);
			this->lToInterpolate->TabIndex = 11;
			this->lToInterpolate->Text = L"interpolate file";
			// 
			// lOutput
			// 
			this->lOutput->AutoSize = true;
			this->lOutput->Location = System::Drawing::Point(12, 66);
			this->lOutput->Name = L"lOutput";
			this->lOutput->Size = System::Drawing::Size(53, 13);
			this->lOutput->TabIndex = 12;
			this->lOutput->Text = L"output file";
			// 
			// lStatus
			// 
			this->lStatus->AutoSize = true;
			this->lStatus->Location = System::Drawing::Point(292, 120);
			this->lStatus->Name = L"lStatus";
			this->lStatus->Size = System::Drawing::Size(0, 13);
			this->lStatus->TabIndex = 13;
			// 
			// progressBar1
			// 
			this->progressBar1->Location = System::Drawing::Point(244, 139);
			this->progressBar1->Name = L"progressBar1";
			this->progressBar1->Size = System::Drawing::Size(100, 17);
			this->progressBar1->TabIndex = 14;
			// 
			// MainForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(356, 165);
			this->Controls->Add(this->progressBar1);
			this->Controls->Add(this->lStatus);
			this->Controls->Add(this->lOutput);
			this->Controls->Add(this->lToInterpolate);
			this->Controls->Add(this->lIntputGrid);
			this->Controls->Add(this->BInterpolate);
			this->Controls->Add(this->TBOutput);
			this->Controls->Add(this->TBToInterpolate);
			this->Controls->Add(this->BOutputFile);
			this->Controls->Add(this->BToInerpolate);
			this->Controls->Add(this->rQuad);
			this->Controls->Add(this->rLine);
			this->Controls->Add(this->rStep);
			this->Controls->Add(this->BInputGrid);
			this->Controls->Add(this->TBInputGrid);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->Name = L"MainForm";
			this->Text = L"MainForm";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void BInputGrid_Click(System::Object^  sender, System::EventArgs^  e)
	{
		String^ path = gcnew String(GetExeDir().c_str());
		OFDInputGrid->InitialDirectory = path;
		OFDInputGrid->FileName = "";
		OFDInputGrid->Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
		OFDInputGrid->FilterIndex = 1;
		OFDInputGrid->RestoreDirectory = true;

		if (OFDInputGrid->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			TBInputGrid->Text = OFDInputGrid->FileName;
			if (TBToInterpolate->Text != "")
			{
				BOutputFile->Enabled = true;
			}
		}
	}

			 inline std::string GetExeDir()
			 {
				 char path[MAX_PATH] = "";
				 GetModuleFileNameA(NULL, path, MAX_PATH);
				 PathRemoveFileSpecA(path);
				 PathAddBackslashA(path);

				 return path;
			 }


	private: System::Void BToInerpolate_Click(System::Object^  sender, System::EventArgs^  e)
	{
		String^ path = gcnew String(GetExeDir().c_str());
		OFDToInterpolate->InitialDirectory = path;
		OFDToInterpolate->FileName = "";
		OFDToInterpolate->Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
		OFDToInterpolate->FilterIndex = 1;
		OFDToInterpolate->RestoreDirectory = true;
		if (OFDToInterpolate->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			TBToInterpolate->Text = OFDToInterpolate->FileName;
			if (TBInputGrid->Text != "")
			{
				BOutputFile->Enabled = true;
			}
		}
	}

	private: System::Void BOutputFile_Click(System::Object^  sender, System::EventArgs^  e)
	{

		String^ path = gcnew String(GetExeDir().c_str());
		SFDOutput->InitialDirectory = path;
		SFDOutput->FileName = "Results.txt";
		SFDOutput->Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
		SFDOutput->FilterIndex = 1;
		SFDOutput->RestoreDirectory = true;
		if (SFDOutput->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			TBOutput->Text = SFDOutput->FileName;
			BInterpolate->Enabled = true;
		}
	}

	private: System::Void BInterpolate_Click(System::Object^  sender, System::EventArgs^  e)
	{

		MATRIX loadedDataGrid = TLoader::LoadFromFile(msclr::interop::marshal_as<std::string>(OFDInputGrid->FileName->ToString()), false);
		MATRIX loadedToInterpolate = TLoader::LoadFromFile(msclr::interop::marshal_as<std::string>(OFDToInterpolate->FileName->ToString()), true);

		TAbstractInterpolator* interpolator = nullptr;

		if (rStep->Checked) interpolator = new TStepInterpolator();
		if (rLine->Checked) interpolator = new TLineInterpolator();
		if (rQuad->Checked) interpolator = new TQuadraticInterpolator();
		
		lStatus->Text = "";

		progressBar1->Minimum = 0;
		progressBar1->Maximum = loadedToInterpolate.ColCount;
		progressBar1->Value = progressBar1->Minimum;

		try
		{
			for (int i = 0; i < loadedToInterpolate.ColCount; i++)
			{
				loadedToInterpolate[i] = (interpolator->interpolate(loadedDataGrid, loadedToInterpolate[i][0])).V;
				progressBar1->Value++;
			}

			TLoader::SaveToFile(msclr::interop::marshal_as<std::string>(SFDOutput->FileName->ToString()), loadedToInterpolate);
			progressBar1->Text = "done!";
			lStatus->Text = "done!";
		}
		catch (const std::exception& ex)
		{
			String^ text = gcnew String(ex.what());
			if (MessageBox::Show(text,"", MessageBoxButtons::OK,
				MessageBoxIcon::Error) == System::Windows::Forms::DialogResult::OK);
			lStatus->Text = "error!";
		}
	}
	};
}
