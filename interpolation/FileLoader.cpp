#include "FileLoader.h"
#include <fstream>

MATRIX TLoader::LoadFromFile(std::string FilePath, bool IsonlyX)
{
	int countElems, colColumns;
	MATRIX result;
	std::ifstream input(const_cast<char*>(FilePath.c_str()));

	IsonlyX ? colColumns = 1 : colColumns = 2;

	input >> countElems;
	result = MATRIX(countElems, 2);
	for (int i = 0; i < countElems; i++)
		for (int j = 0; j < colColumns; j++)
			input >> result[i][j];

	input.close();

	return result;
}

void TLoader::SaveToFile(std::string FilePath, MATRIX Data)
{
	std::ofstream output(const_cast<char*>(FilePath.c_str()));

	output << Data.ColCount << std::endl;
	for (int i = 0; i < Data.ColCount; i++)
	{
		for (int j = 0; j < Data[j].size(); j++)
		{
			output << Data[i][j] << "  ";
		}
		output << std::endl;
	}
	output.close();
}
