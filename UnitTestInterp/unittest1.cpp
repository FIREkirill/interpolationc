#include "CppUnitTest.h"
#include "../interpolationLib/interpolationLib.h"


#define ZERO 1e-5
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Interpolation;
namespace UnitTestInterp
{		
	TEST_CLASS(TestInterpolateMethods)
	{
	public:
		
		TEST_METHOD(TestStepInterpolate)
		{
			TAbstractInterpolator* interpolator = nullptr;

			interpolator = new TStepInterpolator();
			MATRIX interpGrid(3, 2);
			interpGrid[0][0] = 0; interpGrid[0][1] = 0;
			interpGrid[1][0] = 1; interpGrid[1][1] = 1;
			interpGrid[2][0] = 4; interpGrid[2][1] = 16;

			MATRIX toInterp(3, 2);
			toInterp[0][0] = 0.5;
			toInterp[1][0] = 2.1;
			toInterp[2][0] = 3;

			for (int i = 0; i < toInterp.ColCount; i++)
			{
				toInterp[i] = (interpolator->interpolate(interpGrid, toInterp[i][0])).V;
			}

			Assert::IsTrue(abs(toInterp[0][1] - 1) < ZERO);
			Assert::IsTrue(abs(toInterp[1][1] - 16) < ZERO);
			Assert::IsTrue(abs(toInterp[2][1] - 16) < ZERO);
		}

		TEST_METHOD(TestLineInterpolate)
		{
			TAbstractInterpolator* interpolator = nullptr;

			interpolator = new TLineInterpolator();
			MATRIX interpGrid(3, 2);
			interpGrid[0][0] = 0; interpGrid[0][1] = 0;
			interpGrid[1][0] = 1; interpGrid[1][1] = 1;
			interpGrid[2][0] = 4; interpGrid[2][1] = 16;

			MATRIX toInterp(3, 2);
			toInterp[0][0] = 0.5;
			toInterp[1][0] = 2.1;
			toInterp[2][0] = 3;

			for (int i = 0; i < toInterp.ColCount; i++)
			{
				toInterp[i] = (interpolator->interpolate(interpGrid, toInterp[i][0])).V;
			}

			Assert::IsTrue(abs(toInterp[0][1] - 0.5) < ZERO);
			Assert::IsTrue(abs(toInterp[1][1] - 6.5) < ZERO);
			Assert::IsTrue(abs(toInterp[2][1] - 11) < ZERO);
		}

		TEST_METHOD(TestQdraticInterpolate)
		{
			TAbstractInterpolator* interpolator = nullptr;

			interpolator = new TQuadraticInterpolator();
			MATRIX interpGrid(3, 2);
			interpGrid[0][0] = 0; interpGrid[0][1] = 0;
			interpGrid[1][0] = 1; interpGrid[1][1] = 1;
			interpGrid[2][0] = 4; interpGrid[2][1] = 16;

			MATRIX toInterp(3,2);
			toInterp[0][0] = 0.5;
			toInterp[1][0] = 2.1;
			toInterp[2][0] = 3;

			for (int i = 0; i < toInterp.ColCount; i++)
			{
				toInterp[i] = (interpolator->interpolate(interpGrid, toInterp[i][0])).V;
			}

			Assert::IsTrue(abs(toInterp[0][1] - 0.25) < ZERO);
			Assert::IsTrue(abs(toInterp[1][1] - 4.41) < ZERO);
			Assert::IsTrue(abs(toInterp[2][1] - 9) < ZERO);
		}

	};
}