#ifndef INTERP_EXPORT
#define INTERP_API __declspec(dllexport)
#else
#define INTERP_API __declspec(dllimport)
#endif

#include <vector>
#include "TYPES.h"
namespace Interpolation
{
	class TAbstractInterpolator
	{
	protected:
		virtual INTERP_API VECTOR searchLeftPointX(MATRIX interpolGrid, double X);
		virtual INTERP_API VECTOR searchRigtPointhX(MATRIX interpolGrid, double X);
		virtual INTERP_API bool checkPointsInGrid(MATRIX &interpolGrid);
		virtual INTERP_API bool checkRange(MATRIX &interpolGrid, double X);
		virtual INTERP_API bool searchInGrid(MATRIX &interpolGrid, double X, VECTOR& InGrid);
	public:
		virtual INTERP_API VECTOR interpolate(MATRIX &interpolGrid, double X) = 0;

		virtual INTERP_API ~TAbstractInterpolator() {};
	};

	class TStepInterpolator : public TAbstractInterpolator
	{
	private:
		INTERP_API VECTOR nearbyPoint(VECTOR &LeftPoint, VECTOR &RightPoint);
	public:
		INTERP_API TStepInterpolator() {};
		INTERP_API VECTOR interpolate(MATRIX &interpolGrid, double X);
	};

	class TLineInterpolator : public TAbstractInterpolator
	{
	private:
		INTERP_API double calcY(VECTOR &LeftPoint, VECTOR &RightPoint, double X);
	public:
		INTERP_API TLineInterpolator() {};
		INTERP_API VECTOR interpolate(MATRIX &interpolGrid, double X);
	};

	class TQuadraticInterpolator : public TAbstractInterpolator
	{
	private:
		INTERP_API double calcY(VECTOR &FirstPoint, VECTOR &SecondPoint, VECTOR &ThirdPoint, double X);
		virtual INTERP_API VECTOR searchThirdPoint(MATRIX interpolGrid, VECTOR &FirstPoint, VECTOR &SecondPoint);
	protected:
		virtual INTERP_API bool checkPointsInGrid(MATRIX &interpolGrid);
	public:
		INTERP_API TQuadraticInterpolator() {};
		INTERP_API VECTOR interpolate(MATRIX &interpolGrid, double X);
	};
}