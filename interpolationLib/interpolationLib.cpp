#include "interpolationLib.h"


VECTOR Interpolation::TAbstractInterpolator::searchLeftPointX(MATRIX interpolGrid, double X)
{
	VECTOR result(2);
	result[0] = -FLT_MAX;
	result[1] = -FLT_MAX;
	if (interpolGrid.ColCount > 0)
	{
		for (unsigned i = 0; i < interpolGrid.ColCount; i++)
		{
			if (interpolGrid[i][0] < X)
			{
				result = interpolGrid[i];
			}
			else
				break;
		}
	}

	return result;
}

VECTOR Interpolation::TAbstractInterpolator::searchRigtPointhX(MATRIX interpolGrid, double X)
{
	VECTOR result(2);
	result[0] = -FLT_MAX;
	result[1] = -FLT_MAX;

	if (interpolGrid.ColCount > 0)
	{
		for (unsigned i = interpolGrid.ColCount - 1; i >= 0; i--)
		{
			if (interpolGrid[i][0] > X)
			{
				result = interpolGrid[i];
			}
			else
				break;
		}
	}
	return result;
}

bool Interpolation::TAbstractInterpolator::checkPointsInGrid(MATRIX & interpolGrid)
{
	if (interpolGrid.ColCount >= 2)
	{
		return true;
	}
	else
	{
		throw std::exception("count of points in interpolated grid < 2");
		return false;
	}

}

bool Interpolation::TAbstractInterpolator::checkRange(MATRIX & interpolGrid, double X)
{
	if (X >= interpolGrid[0][0] && X <= interpolGrid[interpolGrid.ColCount - 1][0])
	{
		return true;
	}
	else
	{
		throw std::exception("point not in interpolation range");
		return false;
	}
	
}

INTERP_API bool Interpolation::TAbstractInterpolator::searchInGrid(MATRIX & interpolGrid, double X, VECTOR & InGrid)
{
	for (int i = 0; i < interpolGrid.ColCount; i++)
	{
		if (interpolGrid[i][0] == X)
		{
			InGrid = interpolGrid[i];
			return true;
		}
	}
	return false;
}

VECTOR Interpolation::TStepInterpolator::nearbyPoint(VECTOR &LeftPoint, VECTOR &RightPoint)
{
	return (LeftPoint[0] > RightPoint[0]) ? LeftPoint : RightPoint;
}

VECTOR Interpolation::TStepInterpolator::interpolate(MATRIX & interpolGrid, double X)
{
	VECTOR leftPoint, rightPoint, result(2);
	
	if (searchInGrid(interpolGrid, X, result))
		return result;

	if (checkPointsInGrid(interpolGrid) && checkRange(interpolGrid, X))
	{
		leftPoint = searchLeftPointX(interpolGrid, X);
		rightPoint = searchRigtPointhX(interpolGrid, X);
		result = nearbyPoint(leftPoint, rightPoint);
		result[0] = X;
	}
	return result;
}

double Interpolation::TLineInterpolator::calcY(VECTOR & LeftPoint, VECTOR & RightPoint, double X)
{
	double result = RightPoint[1] + ((LeftPoint[1] - RightPoint[1]) / (LeftPoint[0] - RightPoint[0])) * (X - RightPoint[0]);
	return result;
}

VECTOR Interpolation::TLineInterpolator::interpolate(MATRIX & interpolGrid, double X)
{

	VECTOR leftPoint, rightPoint, result(2);
	
	if (searchInGrid(interpolGrid, X, result))
		return result;

	if (checkPointsInGrid(interpolGrid) && checkRange(interpolGrid, X))
	{
		leftPoint = searchLeftPointX(interpolGrid, X);
		rightPoint = searchRigtPointhX(interpolGrid, X);
		result[1] = calcY(leftPoint, rightPoint, X);
		result[0] = X;
	}
	return result;
}

double Interpolation::TQuadraticInterpolator::calcY(VECTOR & FirstPoint, VECTOR & SecondPoint, VECTOR & ThirdPoint, double X)
{
	double result = (((X - SecondPoint[0])*(X - ThirdPoint[0])) / ((FirstPoint[0] - SecondPoint[0])*(FirstPoint[0] - ThirdPoint[0])))*FirstPoint[1] +
		(((X - FirstPoint[0])*(X - ThirdPoint[0])) / ((SecondPoint[0] - FirstPoint[0])*(SecondPoint[0] - ThirdPoint[0])))*SecondPoint[1] +
		(((X - FirstPoint[0])*(X - SecondPoint[0])) / ((ThirdPoint[0] - FirstPoint[0])*(ThirdPoint[0] - SecondPoint[0])))*ThirdPoint[1];
	return result;
}

VECTOR Interpolation::TQuadraticInterpolator::searchThirdPoint(MATRIX interpolGrid, VECTOR & FirstPoint, VECTOR & SecondPoint)
{
	VECTOR result = searchRigtPointhX(interpolGrid, SecondPoint[0]);
	if (result[0] != -FLT_MAX)
	{
		return result;
	}
	else
	{
		result = searchLeftPointX(interpolGrid, FirstPoint[0]);
		if (result[0] != -FLT_MAX)
		{
			VECTOR temp = SecondPoint;
			SecondPoint = FirstPoint;
			FirstPoint = result;

			return temp;
		}
	}
	return result;
}

bool Interpolation::TQuadraticInterpolator::checkPointsInGrid(MATRIX & interpolGrid)
{
	if (interpolGrid.ColCount >= 3)
	{
		return true;
	}
	else
	{
		throw std::exception("count of points in interpolated grid < 3");
		return false;
	}
}
VECTOR Interpolation::TQuadraticInterpolator::interpolate(MATRIX & interpolGrid, double X)
{
	VECTOR firstPoint, secondPoint, thirdPoint, result(2);
	
	if (searchInGrid(interpolGrid, X, result))
		return result;

	if (checkPointsInGrid(interpolGrid) && checkRange(interpolGrid, X))
	{
		firstPoint = searchLeftPointX(interpolGrid, X);
		secondPoint = searchRigtPointhX(interpolGrid, X);
		thirdPoint = searchThirdPoint(interpolGrid, firstPoint, secondPoint);

		result[1] = calcY(firstPoint, secondPoint, thirdPoint, X);
		result[0] = X;
	}
	return result;
}

