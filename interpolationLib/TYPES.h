#ifndef INCLUDE_TYPES_H
#define INCLUDE_TYPES_H
//-------------------------------------------------------------------
//-------------------------------------------------------------------
#include "vector"
#include <math.h>

typedef  std::vector <double> TVector;
typedef std::vector <std::vector <double> > TMatrix;

class VECTOR
{
public:
	TVector V;
	 unsigned int length;

	VECTOR(TVector input)
		{
			V.resize(input.size());
			V = input;
			length = V.size();
		}

	VECTOR(unsigned int size)
		{
			V.resize(size);
			length = size;
		}
	VECTOR()
		{}

	void resize(int size)
		{
		V.resize(size);
		length = size;
		}

    double &operator[](int element)
		{
		return  V[element];
		}
};

class MATRIX
{
public:
	TMatrix MData;
	unsigned int RowCount, ColCount;

	MATRIX(int cols, int rows)
		{
			MData.resize(cols);
			for (unsigned i = 0; i < cols; i++)
				MData[i].resize(rows);
			ColCount = cols;
			RowCount = rows;
		}

	MATRIX(TMatrix Arr)
	{
		RowCount = Arr[0].size();
		ColCount = Arr.size();
		MData.resize(ColCount);
		for (int i = 0; i < ColCount; i++)
			MData[i].resize(RowCount);
		for (int i = 0; i < RowCount; i++)
		for (int j = 0; j < ColCount; j++)
			MData[i][j] = Arr[i][j];
	};

	MATRIX(MATRIX &Arr)
	{
		RowCount = Arr.GetRowCount();
		ColCount = Arr.GetColCount();
		MData.resize(ColCount);
		for (int i = 0; i < ColCount; i++)
			MData[i].resize(RowCount);
		for (int i = 0; i < ColCount; i++)
		for (int j = 0; j < RowCount; j++)
			MData[i][j] = Arr[i][j];
	}

	MATRIX()
		{}

		void resize(int cols, int rows)
		{
			MData.resize(cols);
			for (unsigned i = 0; i < cols; i++)
				MData[i].resize(rows);
			ColCount = cols;
			RowCount = rows;
		}
		
	int GetRowCount()
		{
			return RowCount;
		}

	int GetColCount()
		{
			return ColCount;
		}
	bool Square()                     
		{
			return RowCount = ColCount;
		}

	void ReSize(int i, int j)       // задание размерности матрицы
	{
		ColCount = i;
		RowCount = j;
		MData.resize(ColCount);
		for (int i = 0; i < ColCount; i++)
			MData[i].resize(RowCount);
	}

	TVector &operator[](int col) 
		{
		return MData[col];
		}
};


//-------------------------------------------------------------------



#endif//INCLUDE_TYPES_H
